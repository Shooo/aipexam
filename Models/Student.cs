using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Exam.Models {
    public class Student {
        [Key]
        public int Id {get; set;}
        public string FirstName {get; set;}
        public string FamilyName {get; set;}
        public string GroupName {get; set;}
        public virtual IList<Grade> Grades {get; set;}
    }
}
