using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exam.Models {
    public class Grade {
        [Key]
        public int Id {get; set;}
        public string Description {get; set;}

        public int StudentId {get; set;}
        [ForeignKey("StudentId")]
        public virtual Student Student {get; set;}
        public int Value {get; set;}
    }
}
